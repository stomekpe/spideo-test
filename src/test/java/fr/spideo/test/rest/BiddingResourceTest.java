package fr.spideo.test.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

import java.time.ZonedDateTime;
import java.util.Comparator;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import fr.spideo.test.TestConfig;
import fr.spideo.test.config.AppData;
import fr.spideo.test.domain.Auction;
import fr.spideo.test.domain.AuctionHouse;
import fr.spideo.test.domain.Bidding;
import fr.spideo.test.exception.BadRequestException;
import fr.spideo.test.exception.ErrorMessageEnum;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class)
public class BiddingResourceTest {

    @Autowired
    BiddingResource biddingResource;

    @Autowired
    AppData appData;

    Set<AuctionHouse> auctionHouses = new HashSet<>();
    Set<Auction> auctionHouses1Auctions = new HashSet<>();
    Set<Auction> auctionHouses2Auctions = new HashSet<>();
    Set<Bidding> auction1Biddings = new HashSet<>();
    Set<Bidding> auction2Biddings = new HashSet<>();



    @BeforeEach
    public void populateData() {
        ZonedDateTime now = ZonedDateTime.now();
        appData.getAuctionHouses().clear();
        AuctionHouse auctionHouse1 = new AuctionHouse(1, "auction 1");
        Auction auction1 = new Auction(1, "name1", "description1", now.minusDays(10), now.minusDays(9), 10L, 60L,  Boolean.FALSE);
        auction1Biddings.add(new Bidding(1, "user1", 11L));
        auction1Biddings.add(new Bidding(2, "user2", 20L));
        auction1Biddings.add(new Bidding(3, "user1", 30L));
        auction1Biddings.add(new Bidding(4, "user3", 60L));
        auction1.getBiddings().addAll(auction1Biddings);
        auctionHouses1Auctions.add(auction1);
        Auction auction2 = new Auction(2, "name2", "description2", now.minusDays(1), now.plusMinutes(10), 30L, 60L,  Boolean.FALSE);
        auctionHouses1Auctions.add(auction2);
        auction2Biddings.add(new Bidding(5, "user1", 11L));
        auction2Biddings.add(new Bidding(6, "user2", 20L));
        auction2Biddings.add(new Bidding(7, "user1", 30L));
        auction2Biddings.add(new Bidding(8, "user3", 60L));
        auction2.getBiddings().addAll(auction2Biddings);
        auctionHouses1Auctions.add(new Auction(3, "name3", "description3", now.plusDays(1), now.plusDays(2), 30L, 30L,  Boolean.FALSE));
        auctionHouses1Auctions.add(new Auction(4, "name4", "description4", now.plusDays(1), now.plusDays(2), 30L, 30L,  Boolean.TRUE));
        auctionHouse1.getAuctions().addAll(auctionHouses1Auctions);
        auctionHouses.add(auctionHouse1);
        AuctionHouse auctionHouse2 = new AuctionHouse(2, "auction 2");
        auctionHouses.add(auctionHouse2);
        auctionHouses2Auctions.add(new Auction(5, "name5", "description5", now.minusDays(10), now.minusDays(9), 10L, 10L,  Boolean.FALSE));
        auctionHouse2.getAuctions().addAll(auctionHouses2Auctions);
        auctionHouses.add(new AuctionHouse(3, "auction 3"));
        appData.getAuctionHouses().addAll(auctionHouses);

    }

    @Test
    public void whenFindByAuctionIdAndUserNameUser1ThenOKTest() {
        String userName = "user1";
        ResponseEntity<Set<Bidding>> results = biddingResource.findByUserNameAndAuctionId(1, userName );
        assertThat(results).isNotNull();
        assertThat(results.getBody()).containsExactlyInAnyOrderElementsOf(auction1Biddings.stream().filter(b->userName.equals(b.getUserName())).collect(Collectors.toSet()));
    }
    
    @Test
    public void whenFindByAuctionIdNullAndUserNameUser1ThenIllegalArgumenteExceptionTest() {
        try {
            String userName = "user1";
            biddingResource.findByUserNameAndAuctionId(null, userName );
            fail("The test must fail");
        } catch (IllegalArgumentException ex) {
            assertThat(ex.getMessage()).isEqualTo(ErrorMessageEnum.AUCTION_ID_NULL.getMessage());

        } catch (Exception e) {
            fail("The exception must be IllegalArgumenteException");
        }
    }
    @Test
    public void whenFindByAuctionIdNotExistAndUserNameUser1ThenNoSuchElementExceptionTest() {
        try {
            String userName = "user1";
            biddingResource.findByUserNameAndAuctionId(20, userName );
            fail("The test must fail");
        } catch (NoSuchElementException ex) {
            assertThat(ex.getMessage()).isEqualTo(ErrorMessageEnum.AUCTION_NOT_FOUND.getMessage());

        } catch (Exception e) {
            fail("The exception must be NoSuchElementException");
        }
    }
    
    @Test
    public void whenGetWinnerAuctionIdTerminatedThenOKTest() {
        ResponseEntity<Bidding> results = biddingResource.getWinner(1);
        assertThat(results).isNotNull();
        assertThat(results.getBody()).isEqualTo(auction1Biddings.stream().max(Comparator.comparing(Bidding::getAmount)).get());
    }
    
    @Test
    public void whenGetWinnerAuctionIdNullThenIllegalArgumenteExceptionTest() {
        try {
            biddingResource.getWinner(null);
            fail("The test must fail");
        } catch (IllegalArgumentException ex) {
            assertThat(ex.getMessage()).isEqualTo(ErrorMessageEnum.AUCTION_ID_NULL.getMessage());

        } catch (Exception e) {
            fail("The exception must be IllegalArgumenteException");
        }
    }
    @Test
    public void whenGetWinnerAuctionIdNotExistThenNoSuchElementExceptionTest() {
        try {
            biddingResource.getWinner(20);
            fail("The test must fail");
        } catch (NoSuchElementException ex) {
            assertThat(ex.getMessage()).isEqualTo(ErrorMessageEnum.AUCTION_NOT_FOUND.getMessage());

        } catch (Exception e) {
            fail("The exception must be NoSuchElementException");
        }
    }
    
    @Test
    public void whenGetWinnerAuctionIdRunningThenBadRequestExceptionTest() {
        try {
            biddingResource.getWinner(2);
            fail("The test must fail");
        } catch (BadRequestException ex) {
            assertThat(ex.getMessage()).isEqualTo(ErrorMessageEnum.AUCTION_NOT_TERMINATED.getMessage());

        } catch (Exception e) {
            fail("The exception must be BadRequestException");
        }
    }
    
    @Test
    public void whenCreateThenOKTest() {
        Bidding biddingToCreate = new Bidding(null, "user4", 70L);
        Integer auctionId = 2;

        biddingResource.create(auctionId, biddingToCreate);
        Optional<Auction> auction2 = appData.getAuctionHouses().stream().flatMap(ah -> ah.getAuctions().stream()).filter(a -> auctionId.equals(a.getId())).findFirst();
        assertThat(auction2).isPresent();
        assertThat(auction2.get().getBiddings()).hasSize(auction2Biddings.size() + 1);
        assertThat(auction2.get().getBiddings()).extracting("userName").contains(biddingToCreate.getUserName());
        assertThat(auction2.get().getCurrentPrice()).isEqualTo(biddingToCreate.getAmount());

    }
    
    @Test
    public void whenCreateWithLowerAmountThenBadRequestExceptionTest() {
        Bidding biddingToCreate = new Bidding(null, "user4", 60L);
        Integer auctionId = 2;
        try {
            biddingResource.create(auctionId, biddingToCreate);
            fail("The test must fail");
        } catch (BadRequestException ex) {
            assertThat(ex.getMessage()).isEqualTo(ErrorMessageEnum.BIDDING_AMMOUNT_LOWER_THAN_CURRENT_PRICE.getMessage());

        } catch (Exception e) {
            fail("The exception must be BadRequestException");
        }
    }

    @Test
    public void whenCreateOnAuctionNotRunningThenBadRequestExceptionTest() {
        Bidding biddingToCreate = new Bidding(null, "user4", 70L);
        Integer auctionId = 1;
        try {
            biddingResource.create(auctionId, biddingToCreate);
            fail("The test must fail");
        } catch (BadRequestException ex) {
            assertThat(ex.getMessage()).isEqualTo(ErrorMessageEnum.AUCTION_NOT_RUNNING.getMessage());

        } catch (Exception e) {
            fail("The exception must be BadRequestException");
        }
    }
    
    @Test
    public void whenCreateAuctionIdNotExistThenNoSuchElementExceptionTest() {
        Bidding biddingToCreate = new Bidding(null, "user4", 70L);
        Integer auctionId = 20;
        try {
            biddingResource.create(auctionId, biddingToCreate);
            fail("The test must fail");
        } catch (NoSuchElementException ex) {
            assertThat(ex.getMessage()).isEqualTo(ErrorMessageEnum.AUCTION_NOT_FOUND.getMessage());

        } catch (Exception e) {
            fail("The exception must be NoSuchElementException");
        }
    }
    
    @Test
    public void whenCreateAuctionIdNullThenIllegalArgumenteExceptionTest() {
        Bidding biddingToCreate = new Bidding(null, "user4", 70L);
        Integer auctionId = null;
        try {
            biddingResource.create(auctionId, biddingToCreate);
            fail("The test must fail");
        } catch (IllegalArgumentException ex) {
            assertThat(ex.getMessage()).isEqualTo(ErrorMessageEnum.AUCTION_ID_NULL.getMessage());

        } catch (Exception e) {
            fail("The exception must be IllegalArgumenteException");
        }
    }
    
    @Test
    public void whenCreateWithIdThenIllegalArgumenteExceptionTest() {
        Bidding biddingToCreate = new Bidding(1, "user4", 70L);
        Integer auctionId = 2;
        try {
            biddingResource.create(auctionId, biddingToCreate);
            fail("The test must fail");
        } catch (IllegalArgumentException ex) {
            assertThat(ex.getMessage()).isEqualTo(ErrorMessageEnum.ID_NOT_NULL.getMessage());

        } catch (Exception e) {
            fail("The exception must be IllegalArgumenteException");
        }
    }
    
}
