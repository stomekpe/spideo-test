package fr.spideo.test.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import fr.spideo.test.TestConfig;
import fr.spideo.test.config.AppData;
import fr.spideo.test.domain.AuctionHouse;
import fr.spideo.test.exception.ErrorMessageEnum;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class)
public class AuctionHouseResourceTest {

    @Autowired
    AuctionHouseResource auctionHouseResource;

    @Autowired
    AppData appData;

    Set<AuctionHouse> auctionHouses = new HashSet<>();

    @BeforeEach
    public void populateData() {
        appData.getAuctionHouses().clear();
        auctionHouses.add(new AuctionHouse(1, "auction 1"));
        auctionHouses.add(new AuctionHouse(2, "auction 2"));
        auctionHouses.add(new AuctionHouse(3, "auction 3"));
        appData.getAuctionHouses().addAll(auctionHouses);

    }

    @Test
    public void whenGetAllThenOKTest() {
        ResponseEntity<Set<AuctionHouse>> results = auctionHouseResource.getAll();
        assertThat(results).isNotNull();
        assertThat(results.getBody()).containsExactlyInAnyOrderElementsOf(auctionHouses);
    }

    @Test
    public void whenCreateWithIdThenIllegalArgumenteExceptionTest() {
        try {
            auctionHouseResource.create(new AuctionHouse(1, "name"));
            fail("The test must fail");
        } catch (IllegalArgumentException ex) {
            assertThat(ex.getMessage()).isEqualTo( ErrorMessageEnum.ID_NOT_NULL.getMessage());

        } catch (Exception e) {
            fail("The exception must be IllegalArgumenteException");
        }

    }

    @Test
    public void whenCreateWithoutIdThenOKTest() {
        AuctionHouse auctionHouse = new AuctionHouse(null, "name");
        ResponseEntity<AuctionHouse> results = auctionHouseResource.create(auctionHouse);
        assertThat(results).isNotNull();
        assertThat(results.getBody()).usingRecursiveComparison().ignoringFields("id").isEqualTo(auctionHouse);
        assertThat(results.getBody().getId()).isNotNull();
    }

    @Test
    public void whenDeleteNotExistThenNoSuchElementExceptionTest() {
        try {
            auctionHouseResource.delete(20);
            fail("The test must fail");
        } catch (NoSuchElementException ex) {
            assertThat(ex.getMessage()).isEqualTo(ErrorMessageEnum.AUCTION_HOUSE_NOT_FOUND.getMessage());

        } catch (Exception e) {
            fail("The exception must be NoSuchElementException");
        }
    }

    @Test
    public void whenDeleteWithIdNullThenIllegalArgumenteExceptionTest() {
        try {
            auctionHouseResource.delete(null);
            fail("The test must fail");
        } catch (IllegalArgumentException ex) {
            assertThat(ex.getMessage()).isEqualTo(ErrorMessageEnum.AUCTION_HOUSE_ID_NULL.getMessage());
        } catch (Exception e) {
            fail("The exception must be IllegalArgumenteException");
        }

    }

    @Test
    public void whenDeleteThenOKTest() {
        auctionHouseResource.delete(2);
        assertThat(appData.getAuctionHouses()).hasSize(2);
        assertThat(appData.getAuctionHouses()).extracting("id").doesNotContain(2);

    }

}
