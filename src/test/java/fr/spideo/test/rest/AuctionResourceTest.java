package fr.spideo.test.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import fr.spideo.test.TestConfig;
import fr.spideo.test.config.AppData;
import fr.spideo.test.domain.Auction;
import fr.spideo.test.domain.AuctionHouse;
import fr.spideo.test.domain.AuctionStatusEnum;
import fr.spideo.test.exception.BadRequestException;
import fr.spideo.test.exception.ErrorMessageEnum;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class)
public class AuctionResourceTest {

    @Autowired
    AuctionResource auctionResource;

    @Autowired
    AppData appData;

    Set<AuctionHouse> auctionHouses = new HashSet<>();
    Set<Auction> auctionHouses1Auctions = new HashSet<>();
    Set<Auction> auctionHouses2Auctions = new HashSet<>();



    @BeforeEach
    public void populateData() {
        ZonedDateTime now = ZonedDateTime.now();
        appData.getAuctionHouses().clear();
        AuctionHouse auctionHouse1 = new AuctionHouse(1, "auction 1");
        auctionHouses1Auctions.add(new Auction(1, "name1", "description1", now.minusDays(10), now.minusDays(9), 10L, 60L,  Boolean.FALSE));
        auctionHouses1Auctions.add(new Auction(2, "name2", "description2", now.minusDays(1), now.plusMinutes(10), 30L, 60L,  Boolean.FALSE));
        auctionHouses1Auctions.add(new Auction(3, "name3", "description3", now.plusDays(1), now.plusDays(2), 30L, 30L,  Boolean.FALSE));
        auctionHouses1Auctions.add(new Auction(4, "name4", "description4", now.plusDays(1), now.plusDays(2), 30L, 30L,  Boolean.TRUE));

        auctionHouse1.getAuctions().addAll(auctionHouses1Auctions);
        auctionHouses.add(auctionHouse1);
        AuctionHouse auctionHouse2 = new AuctionHouse(2, "auction 2");
        auctionHouses.add(auctionHouse2);
        auctionHouses2Auctions.add(new Auction(5, "name5", "description5", now.minusDays(10), now.minusDays(9), 10L, 10L,  Boolean.TRUE));
        auctionHouses2Auctions.add(new Auction(6, "name6", "description6", now.minusDays(1), now.plusDays(1), 30L, 30L,  Boolean.TRUE));
        auctionHouses2Auctions.add(new Auction(7, "name7", "description7", now.plusDays(1), now.plusDays(2), 30L, 30L,  Boolean.TRUE));
        auctionHouses2Auctions.add(new Auction(8, "name8", "description8", now.minusDays(13), now.minusDays(12), 10L, 60L,  Boolean.FALSE));
        auctionHouses2Auctions.add(new Auction(9, "name9", "description9", now.minusMinutes(10), now.plusDays(1), 30L, 60L,  Boolean.FALSE));
        auctionHouse2.getAuctions().addAll(auctionHouses2Auctions);
        auctionHouses.add(new AuctionHouse(3, "auction 3"));
        appData.getAuctionHouses().addAll(auctionHouses);

    }

    @Test
    public void whenFindByAuctionId2ThenOKTest() {
        ResponseEntity<Set<Auction>> results = auctionResource.findByAuctionHouseId(2);
        assertThat(results).isNotNull();
        assertThat(results.getBody()).containsExactlyInAnyOrderElementsOf(auctionHouses2Auctions);
    }
    
    @Test
    public void whenFindByAuctionIdNullThenIllegalArgumenteExceptionTest() {
        try {
            auctionResource.findByAuctionHouseId(null);
            fail("The test must fail");
        } catch (IllegalArgumentException ex) {
            assertThat(ex.getMessage()).isEqualTo(ErrorMessageEnum.AUCTION_HOUSE_ID_NULL.getMessage());

        } catch (Exception e) {
            fail("The exception must be IllegalArgumenteException");
        }
    }
    
    @Test
    public void whenFindByStatusDeletedThenOKTest() {
        ResponseEntity<Set<Auction>> results = auctionResource.findByStatus(AuctionStatusEnum.DELETED);
        assertThat(results).isNotNull();
        assertThat(results.getBody()).hasSize(4);
    }
    @Test
    public void whenFindByStatusNotStartedThenOKTest() {
        ResponseEntity<Set<Auction>> results = auctionResource.findByStatus(AuctionStatusEnum.NOT_STARTED);
        assertThat(results).isNotNull();
        assertThat(results.getBody()).hasSize(1);
    }
    
    @Test
    public void whenFindByStatusRunningThenOKTest() {
        ResponseEntity<Set<Auction>> results = auctionResource.findByStatus(AuctionStatusEnum.RUNNING);
        assertThat(results).isNotNull();
        assertThat(results.getBody()).hasSize(2);
    }
    
    @Test
    public void whenFindByStatusTerminatedThenOKTest() {
        ResponseEntity<Set<Auction>> results = auctionResource.findByStatus(AuctionStatusEnum.TERMINATED);
        assertThat(results).isNotNull();
        assertThat(results.getBody()).hasSize(2);
    }
    
    @Test
    public void whenFindStatusNullThenIllegalArgumenteExceptionTest() {
        try {
            auctionResource.findByStatus(null);
            fail("The test must fail");
        } catch (IllegalArgumentException ex) {
            assertThat(ex.getMessage()).isEqualTo(ErrorMessageEnum.STATUS_NULL.getMessage());

        } catch (Exception e) {
            fail("The exception must be IllegalArgumenteException");
        }
    }
    
    @Test
    public void whenUpdateStatusToDeletedNotExistThenNoSuchElementExceptionTest() {
        Set<PatchOperation> operations = new HashSet<>();
        operations.add(new  PatchOperation(PatchOperationEnum.DELETION));
        try {
            auctionResource.updateStatusToDeleted(20,operations);
            fail("The test must fail");
        } catch (NoSuchElementException ex) {
            assertThat(ex.getMessage()).isEqualTo(ErrorMessageEnum.AUCTION_NOT_FOUND.getMessage());

        } catch (Exception e) {
            fail("The exception must be NoSuchElementException");
        }
    }

    @Test
    public void whenUpdateStatusToDeletedWithIdNullThenIllegalArgumenteExceptionTest() {
        try {
            Set<PatchOperation> operations = new HashSet<>();
            operations.add(new  PatchOperation(PatchOperationEnum.DELETION));
            auctionResource.updateStatusToDeleted(null,operations);
            fail("The test must fail");
        } catch (IllegalArgumentException ex) {
            assertThat(ex.getMessage()).isEqualTo(ErrorMessageEnum.AUCTION_ID_NULL.getMessage());
        } catch (Exception e) {
            fail("The exception must be IllegalArgumenteException");
        }

    }

    @Test
    public void whenUpdateStatusToDeletedThenOKTest() {
        Set<PatchOperation> operations = new HashSet<>();
        operations.add(new  PatchOperation(PatchOperationEnum.DELETION));
        Integer auctionToDeleteId = 7;
        auctionResource.updateStatusToDeleted(auctionToDeleteId,operations);
        Optional<Auction> deletedAuction = appData.getAuctionHouses().stream().flatMap(ah->ah.getAuctions().stream()).filter(a->auctionToDeleteId.equals(a.getId())).findFirst();
        assertThat(deletedAuction).isPresent();
        assertThat(deletedAuction.get().getStatus()).isEqualTo(AuctionStatusEnum.DELETED);

    }
    
    @Test
    public void whenCreateThenOKTest() {
        Auction auctionToCreate = new Auction(null, "nameToCreate", "descriptionToCreate", ZonedDateTime.now(), ZonedDateTime.now().plusDays(1), 10L, 10L,null);
        Integer auctionHouseId = 1;
        auctionResource.create(auctionHouseId,auctionToCreate);
        Optional<AuctionHouse> auctionHouse1 = appData.getAuctionHouses().stream().filter(ah->auctionHouseId.equals(ah.getId())).findFirst();
        assertThat(auctionHouse1).isPresent();
        assertThat(auctionHouse1.get().getAuctions()).hasSize(auctionHouses1Auctions.size()+1);
        assertThat(auctionHouse1.get().getAuctions()).extracting("name").contains(auctionToCreate.getName());
          
    }
    
    @Test
    public void whenCreateWithNotExistAuctionHouseIdThenNoSuchElementExceptionTest() {
        try {
            auctionResource.create(20,new Auction(null, "name", "description", ZonedDateTime.now(), ZonedDateTime.now().plusDays(1), 10L, 10L,null));
            fail("The test must fail");
        } catch (NoSuchElementException ex) {
            assertThat(ex.getMessage()).isEqualTo(ErrorMessageEnum.AUCTION_HOUSE_NOT_FOUND.getMessage());

        } catch (Exception e) {
            fail("The exception must be NoSuchElementException");
        }
    }
    
    @Test
    public void whenCreateWithAuctionHouseIdNullThenIllegalArgumenteExceptionTest() {
        try {
            auctionResource.create(null,new Auction(null, "name", "description", ZonedDateTime.now(), ZonedDateTime.now().plusDays(1), 10L, 10L,null));
            fail("The test must fail");
        } catch (IllegalArgumentException ex) {
            assertThat(ex.getMessage()).isEqualTo(ErrorMessageEnum.AUCTION_HOUSE_ID_NULL.getMessage());
        } catch (Exception e) {
            fail("The exception must be IllegalArgumenteException");
        }
    }

    @Test
    public void whenCreateWithIdThenIllegalArgumenteExceptionTest() {
        try {
            auctionResource.create(1,new Auction(1, "name", "description", ZonedDateTime.now(), ZonedDateTime.now().plusDays(1), 10L, 10L,null));
            fail("The test must fail");
        } catch (IllegalArgumentException ex) {
            assertThat(ex.getMessage()).isEqualTo( ErrorMessageEnum.ID_NOT_NULL.getMessage());
        } catch (Exception e) {
            fail("The exception must be IllegalArgumenteException");
        }
    }
    
    @Test
    public void whenCreateWithStartTimeNullThenBadRequestExceptionTest() {
        try {
            auctionResource.create(1,new Auction(null, "name", "description", null, ZonedDateTime.now().plusDays(1), 10L, 10L,null));
            fail("The test must fail");
        } catch (NullPointerException ex) {
            assertThat(ex.getMessage()).isNotBlank();
        } catch (Exception e) {
            fail("The exception must be NullPointerException");
        }
    }
    
    @Test
    public void whenCreateWithEndTimeNullThenBadRequestExceptionTest() {
        try {
            auctionResource.create(1,new Auction(null, "name", "description", ZonedDateTime.now(), null, 10L, 10L,null));
            fail("The test must fail");
        } catch (NullPointerException ex) {
            assertThat(ex.getMessage()).isNotBlank();
        } catch (Exception e) {
            fail("The exception must be NullPointerException");
        }
    }
    
    @Test
    public void whenCreateWithEndTimeBeforeStartTimeThenBadRequestExceptionTest() {
        try {
            auctionResource.create(1,new Auction(null, "name", "description", ZonedDateTime.now(), ZonedDateTime.now().minusDays(1), 10L, 10L,null));
            fail("The test must fail");
        } catch (BadRequestException ex) {
            assertThat(ex.getMessage()).isEqualTo(ErrorMessageEnum.START_TIME_AFTER_END_TIME.getMessage());
        } catch (Exception e) {
            fail("The exception must be BadRequestException");
        }
    }
    
    @Test
    public void whenCreateWithStartPriceDiffCurentPriceThenBadRequestExceptionTest() {
        try {
            auctionResource.create(1,new Auction(null, "name", "description", ZonedDateTime.now(), ZonedDateTime.now().plusDays(1), 10L, 12L,null));
            fail("The test must fail");
        } catch (BadRequestException ex) {
            assertThat(ex.getMessage()).isEqualTo(ErrorMessageEnum.START_PRICE_DIFFERENTE_CURRENT_PRICE.getMessage());
        } catch (Exception e) {
            fail("The exception must be BadRequestException");
        }
    }


}
