package fr.spideo.test;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

import fr.spideo.test.config.SpringFoxConfig;

@Configuration
@ComponentScan(basePackages = "fr.spideo.test", excludeFilters = @ComponentScan.Filter(classes = { AppConfig.class,
        SpringFoxConfig.class }, type = FilterType.ASSIGNABLE_TYPE) )
public class TestConfig {

}
