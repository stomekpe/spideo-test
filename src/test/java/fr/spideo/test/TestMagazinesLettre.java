package fr.spideo.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;


public class TestMagazinesLettre {
    
    
    //je n'ai écris que la fonction qui permet de vérifier si il peut écrire sa lettre
    public boolean checkLettre (Set<List<String>> magazines, List<String> lettre){
        List<String> worldNotUsed = magazines.stream()
                                             .flatMap(m->m.stream())
                                             .map(String::toUpperCase)
                                             .collect(Collectors.toList());
        return lettre.stream()
                     .map(world->worldNotUsed.remove(world.toUpperCase()))
                     .reduce(Boolean.TRUE, (canBeWrite,magazinesContainWorld)-> canBeWrite && magazinesContainWorld);
    }
    
    @Test
    public void testCheckLettr() {
        Set<List<String>> magazines = new HashSet<>();
        magazines.add(Arrays.asList("ET", "Voila","le","Boulot"));
        magazines.add(Arrays.asList("Test"));
        assertThat(checkLettre(magazines, Arrays.asList("Et","voila"))).isEqualTo(true);
        assertThat(checkLettre(magazines, Arrays.asList("Et","voila","Et"))).isEqualTo(false);
        assertThat(checkLettre(magazines, Arrays.asList("Et","voila","cela"))).isEqualTo(false);
        assertThat(checkLettre(magazines, Arrays.asList("Et","Voila","le", "test"))).isEqualTo(true);
        assertThat(checkLettre(magazines, Arrays.asList("Le","Boulot","ET", "VOILA"))).isEqualTo(true);

    }
    

}
