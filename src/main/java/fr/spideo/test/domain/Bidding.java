package fr.spideo.test.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
@EqualsAndHashCode
public class Bidding {
    
    @ApiModelProperty(value="Bidding identify, must be null in ceation",position=1)
    @JsonProperty(value = "id")
    Integer Id;
    
    @NonNull
    @ApiModelProperty(value="User name", position=2, required=true)
    @JsonProperty(value = "userName")
    String userName;
    
    @NonNull
    @ApiModelProperty(value="User bidding amount", position=3, required=true)
    @JsonProperty(value = "amount")
    Long amount;

}
