package fr.spideo.test.domain;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@RequiredArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
@EqualsAndHashCode
public class AuctionHouse {

    @ApiModelProperty(value="Auction house's identify, must be null in ceation",position=1)
    @JsonProperty(value = "id")
    Integer id;

    @NonNull
    @ApiModelProperty(value="Auction house's name",position=2, required=true)
    @JsonProperty(value = "name")
    String name;

    @ApiModelProperty("Auction's manage by the auction house")
    @JsonIgnore
    Set<Auction> auctions = new HashSet<>();
    
    public void setAuctions(Set<Auction> auctions){
        this.auctions = auctions==null? new HashSet<>() : auctions;
    }
    

    public AuctionHouse(Integer id, String name) {
        super();
        this.id = id;
        this.name = name;
        this.auctions = new HashSet<>();
    }

}
