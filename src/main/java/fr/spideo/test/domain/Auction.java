package fr.spideo.test.domain;

import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@ToString
@Getter
@Setter
@EqualsAndHashCode
public class Auction {

    @ApiModelProperty(value="Auction's identify, must be null in ceation", position=1)
    @JsonProperty(value = "id")
    Integer id;

    @NonNull
    @ApiModelProperty(value="Auction's name", position=2, required=true)
    @JsonProperty(value = "name")
    String name;

    @ApiModelProperty(value="Auction's description", position=3)
    @JsonProperty(value = "description")
    String description;

    @NonNull
    @ApiModelProperty(value="Auction's starting time", position=4, required=true)
    @JsonProperty(value = "startDateTime")
    ZonedDateTime startDateTime;

    @NonNull
    @ApiModelProperty(value="Auction's end time",  position=5, required=true)
    @JsonProperty(value = "endDateTime")
    ZonedDateTime endDateTime;

    @NonNull
    @ApiModelProperty(value="Auction's start price",  position=6,required=true)
    @JsonProperty(value = "startPrice")
    Long startPrice;

    @NonNull
    @ApiModelProperty(value="Auction's current price, must be same as start price in creation", position=7, required=true)
    @JsonProperty(value = "currentPrice")
    Long currentPrice;
    
    @JsonIgnore
    Boolean isDeleted = Boolean.FALSE;
    
    
    @ApiModelProperty(value="Auction's status. It's value can only be set to DELETED by a specific API", position=8, accessMode=AccessMode.READ_ONLY)
    @JsonProperty(value = "status",access=Access.READ_ONLY)
    AuctionStatusEnum status;
    
    @ApiModelProperty("Auction's biggings")
    @JsonIgnore
    Set<Bidding> biddings = new HashSet<>();
    
    public Auction(Integer id, @NonNull String name, String description, @NonNull ZonedDateTime startDateTime, @NonNull ZonedDateTime endDateTime,
            @NonNull Long startPrice, @NonNull Long currentPrice, Boolean isDeleted) {
        super();
        this.id = id;
        this.name = name;
        this.description = description;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.startPrice = startPrice;
        this.currentPrice = currentPrice;
        this.isDeleted = isDeleted == null ? Boolean.FALSE : isDeleted;
        this.biddings = new HashSet<>();
    }
    
    public void setBiddings(Set<Bidding> biddings){
        this.biddings = biddings==null? new HashSet<>() : biddings;
    }
    
    
    public AuctionStatusEnum getStatus() {
        ZonedDateTime now = ZonedDateTime.now();
        
        if(isDeleted!=null && isDeleted){
            return AuctionStatusEnum.DELETED;

        }
        if(now.isBefore(startDateTime)){
            return AuctionStatusEnum.NOT_STARTED;
        }
        
        if(now.isAfter(endDateTime)){
            return AuctionStatusEnum.TERMINATED;
        }
        return AuctionStatusEnum.RUNNING;
        
    }
    
    public void setIsDeleted(Boolean isDeleted){
        this.isDeleted = isDeleted==null? Boolean.FALSE : isDeleted;
        
    }



}
