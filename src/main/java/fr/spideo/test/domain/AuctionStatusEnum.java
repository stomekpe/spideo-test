package fr.spideo.test.domain;

public enum AuctionStatusEnum {
    NOT_STARTED, RUNNING,TERMINATED, DELETED;

}
