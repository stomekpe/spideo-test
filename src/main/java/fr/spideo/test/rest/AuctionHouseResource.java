package fr.spideo.test.rest;

import java.util.Set;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.spideo.test.domain.AuctionHouse;
import fr.spideo.test.service.AuctionHouseService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/aution-houses", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class AuctionHouseResource {

    private final AuctionHouseService auctionHouseService;

    @ApiOperation(value = "Get all auction houses")
    @GetMapping(value = "")
    public ResponseEntity<Set<AuctionHouse>> getAll() {
        log.info("Request to get all aution houses");
        Set<AuctionHouse> auctionHouses = auctionHouseService.getAll();
        return ResponseEntity.ok(auctionHouses);
    }

    @ApiOperation(value = "Delete a specific auction house")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable Integer id) {
        log.info("Request to delete aution house {}", id);
        auctionHouseService.delete(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @ApiOperation(value = "Create an aution house ")
    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AuctionHouse> create(@RequestBody AuctionHouse auctionHouse) {
        log.info("Request to create auction house");
        AuctionHouse auctionHouseCreated = auctionHouseService.create(auctionHouse);
        return ResponseEntity.ok(auctionHouseCreated);
    }

}
