package fr.spideo.test.rest;

import java.util.Set;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.spideo.test.domain.Auction;
import fr.spideo.test.domain.AuctionStatusEnum;
import fr.spideo.test.service.AuctionService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class AuctionResource {

    private final AuctionService auctionService;

    @ApiOperation(value = "List all auctions for a given auction house")
    @GetMapping(value = "/aution-houses/{auctionHouseId}/auctions")
    public ResponseEntity<Set<Auction>> findByAuctionHouseId(@PathVariable Integer auctionHouseId) {
        log.info("Request to get aution house's {} auctions ",auctionHouseId);
        Set<Auction> auctions = auctionService.findByAuctionHouseId(auctionHouseId);
        return ResponseEntity.ok(auctions);
    }

    @ApiOperation(value = "Patch a specific auction")
    @PatchMapping(value = "/auctions/{id}")
    public ResponseEntity<Auction> updateStatusToDeleted(@PathVariable Integer id, @RequestBody Set<PatchOperation> operations) {
        log.info("Request to patch an aution {}, operations {}", id,operations);
        Auction auction = auctionService.updateStatusToDeleted(id);
        return ResponseEntity.ok(auction);
    }

    @ApiOperation(value = "Create an aution ")
    @PostMapping(value = "/aution-houses/{auctionHouseId}/auctions", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Auction> create(@PathVariable Integer auctionHouseId, @RequestBody Auction auction) {
        log.info("Request to create auction");
        Auction auctionCreated = auctionService.create(auction,auctionHouseId);
        return ResponseEntity.ok(auctionCreated);
    }
    
    @ApiOperation(value = "List all auctions in a giving status")
    @GetMapping(value = "/auctions")
    public ResponseEntity<Set<Auction>> findByStatus(@RequestParam AuctionStatusEnum status) {
        log.info("Request to get auctions with status ",status);
        Set<Auction> auctions = auctionService.findByAuctionStatus(status);
        return ResponseEntity.ok(auctions);
    }

}
