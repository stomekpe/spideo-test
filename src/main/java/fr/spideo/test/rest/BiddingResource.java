package fr.spideo.test.rest;

import java.util.Optional;
import java.util.Set;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.spideo.test.domain.Bidding;
import fr.spideo.test.service.BiddingService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/autions/{auctionId}/biddings", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class BiddingResource {

    private final BiddingService biddingService;

    @ApiOperation(value = "List all user's bidding  that has happen until now")
    @GetMapping(value = "", params={"userName"})
    public ResponseEntity<Set<Bidding>> findByUserNameAndAuctionId(@PathVariable Integer auctionId, @RequestParam String userName) {
        log.info("Request to get user's {} bidding in auction ",userName,auctionId);
        Set<Bidding> biddings = biddingService.findByUserNameAndAuctionId(userName, auctionId);
        return ResponseEntity.ok(biddings);
    }

    @ApiOperation(value = "Let a specific user bid for a specific auction ")
    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Bidding> create(@PathVariable Integer auctionId, @RequestBody Bidding bidding) {
        log.info("Request to create bidding ");
        Bidding biddingCreated = biddingService.create(bidding,auctionId);
        return ResponseEntity.ok(biddingCreated);
    }
    
    @ApiOperation(value = "get the auction winner")
    @GetMapping(value = "/winner")
    public ResponseEntity<Bidding> getWinner(@PathVariable Integer auctionId) {
        log.info("Request to get auctions {} winner ",auctionId);
        Optional<Bidding> winner = biddingService.getWinner(auctionId);
        return ResponseEntity.of(winner);
    }

}
