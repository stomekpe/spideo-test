package fr.spideo.test.repository.util;

import java.util.concurrent.atomic.AtomicInteger;

public class SequenceGenerator  {

	    private static AtomicInteger sequence = new AtomicInteger(1);

	    
	    public static Integer next() {
	        return sequence.getAndIncrement();
	    }
	}


