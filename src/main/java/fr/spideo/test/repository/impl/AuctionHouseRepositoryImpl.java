package fr.spideo.test.repository.impl;

import java.util.Set;

import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import fr.spideo.test.config.AppData;
import fr.spideo.test.domain.AuctionHouse;
import fr.spideo.test.exception.ErrorMessageEnum;
import fr.spideo.test.repository.AuctionHouseRepository;
import fr.spideo.test.repository.util.SequenceGenerator;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Repository
public class AuctionHouseRepositoryImpl implements AuctionHouseRepository {
	
	private final AppData data;

	@Override
	public AuctionHouse save(AuctionHouse auctionHouse) {
		Assert.isNull(auctionHouse.getId(),  ErrorMessageEnum.ID_NOT_NULL.getMessage());
		auctionHouse.setId(SequenceGenerator.next());
		data.getAuctionHouses().add(auctionHouse);
		return auctionHouse;
	}

	@Override
	public Set<AuctionHouse> getAll() {
		return data.getAuctionHouses();
	}

	@Override
	public Boolean deleteById(Integer id) {
		Assert.notNull(id, ErrorMessageEnum.AUCTION_HOUSE_ID_NULL.getMessage());
		return data.getAuctionHouses().removeIf(a->id.equals(a.getId()));
	}


}
