package fr.spideo.test.repository.impl;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import fr.spideo.test.config.AppData;
import fr.spideo.test.domain.Auction;
import fr.spideo.test.domain.AuctionHouse;
import fr.spideo.test.domain.AuctionStatusEnum;
import fr.spideo.test.exception.ErrorMessageEnum;
import fr.spideo.test.repository.AuctionRepository;
import fr.spideo.test.repository.util.SequenceGenerator;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Repository
public class AuctionRepositoryImpl implements AuctionRepository {

    private final AppData data;

    @Override
    public Auction save(Auction auction, Integer auctionHouseId)  {
        Assert.isNull(auction.getId(),  ErrorMessageEnum.ID_NOT_NULL.getMessage());
        Assert.notNull(auctionHouseId, ErrorMessageEnum.AUCTION_HOUSE_ID_NULL.getMessage());
        Optional<AuctionHouse> auctionHouse = data.getAuctionHouses()
            .stream()
            .filter(ah -> auctionHouseId.equals(ah.getId()))
            .findFirst();
            if(auctionHouse.isPresent()){
                auction.setId(SequenceGenerator.next());
                auctionHouse.get().getAuctions().add(auction);
                return auction;
            }
            throw new NoSuchElementException(ErrorMessageEnum.AUCTION_HOUSE_NOT_FOUND.getMessage());
    }

    @Override
    public Set<Auction> findByAuctionHouseId(Integer auctionHouseId) {
        Assert.notNull(auctionHouseId, ErrorMessageEnum.AUCTION_HOUSE_ID_NULL.getMessage());
        return data.getAuctionHouses()
                   .stream()
                   .filter(ah -> auctionHouseId.equals(ah.getId()))
                   .flatMap(ah -> ah.getAuctions().stream())
                   .collect(Collectors.toSet());

    }

    @Override
    public Optional<Auction> updateStatusToDeleted(Integer id) {
        Assert.notNull(id, ErrorMessageEnum.AUCTION_ID_NULL.getMessage());
        Optional<Auction> auction = data.getAuctionHouses()
        .stream()
        .flatMap(ah -> ah.getAuctions().stream())
        .filter(a->id.equals(a.getId()))
        .findFirst();
       if(auction.isPresent()) {
           auction.get().setIsDeleted(Boolean.TRUE);
       }
      return auction;    
    }

    @Override
    public Set<Auction> findByStatus(AuctionStatusEnum status) {
        Assert.notNull(status, ErrorMessageEnum.STATUS_NULL.getMessage());
        return data.getAuctionHouses()
                   .stream()
                   .flatMap(ah -> ah.getAuctions().stream())
                   .filter(a -> status.equals(a.getStatus()))
                   .collect(Collectors.toSet());
    }

    @Override
    public Optional<Auction> findById(Integer id) {
        Assert.notNull(id, ErrorMessageEnum.AUCTION_ID_NULL.getMessage());
        return data.getAuctionHouses()
                   .stream()
                   .flatMap(ah -> ah.getAuctions().stream())
                   .filter(a -> id.equals(a.getId()))
                   .findFirst();
    }

    @Override
    public void updateAuctionCurrentPrice(Integer id, Long currentPrice)  {
        Assert.notNull(id, ErrorMessageEnum.AUCTION_ID_NULL.getMessage());
        Assert.notNull(currentPrice, ErrorMessageEnum.CURRENT_PRICE_NULL.getMessage());        
        data.getAuctionHouses()
            .stream()
            .flatMap(ah -> ah.getAuctions().stream())
            .filter(a -> id.equals(a.getId()))
            .forEach(a->a.setCurrentPrice(currentPrice));
    }

}
