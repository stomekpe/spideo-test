package fr.spideo.test.repository.impl;

import java.util.Comparator;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import fr.spideo.test.config.AppData;
import fr.spideo.test.domain.Auction;
import fr.spideo.test.domain.Bidding;
import fr.spideo.test.exception.ErrorMessageEnum;
import fr.spideo.test.repository.BiddingRepository;
import fr.spideo.test.repository.util.SequenceGenerator;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Repository
public class BiddingRepositoryImpl implements BiddingRepository {

    private final AppData data;

    @Override
    public Bidding save(Bidding bidding, Integer auctionId)  {
        Assert.isNull(bidding.getId(), ErrorMessageEnum.ID_NOT_NULL.getMessage());
        Assert.notNull(auctionId, ErrorMessageEnum.AUCTION_ID_NULL.getMessage());
        Optional<Auction> auction = data.getAuctionHouses()
            .stream()
            .flatMap(ah-> ah.getAuctions().stream())
            .filter(a -> auctionId.equals(a.getId()))
            .findFirst();
            if(auction.isPresent()){
                bidding.setId(SequenceGenerator.next());
                auction.get().getBiddings().add(bidding);
                return bidding;
            }
        throw new NoSuchElementException(ErrorMessageEnum.AUCTION_NOT_FOUND.getMessage());
    }

    @Override
    public Set<Bidding> finfByUserNameAndAuctionId(String userName, Integer auctionId) {
        Assert.notNull(userName, ErrorMessageEnum.USER_NAME_NULL.getMessage());
        Assert.notNull(auctionId, ErrorMessageEnum.AUCTION_ID_NULL.getMessage());
        return data.getAuctionHouses()
                .stream()
                .flatMap(ah -> ah.getAuctions().stream())
                .filter(a -> auctionId.equals(a.getId()))
                .flatMap(a -> a.getBiddings().stream())
                .filter(b -> userName.equals(b.getUserName()))
                .collect(Collectors.toSet());
    }

    @Override
    public Optional<Bidding> findByAuctionIdAndMaxAmount(Integer auctionId) {
        Assert.notNull(auctionId, ErrorMessageEnum.AUCTION_ID_NULL.getMessage());
        return data.getAuctionHouses()
                .stream()
                .flatMap(ah -> ah.getAuctions().stream())
                .filter(a -> auctionId.equals(a.getId()))
                .flatMap(a -> a.getBiddings().stream())
                .max(Comparator.comparing(Bidding::getAmount));
    }

}
