package fr.spideo.test.repository;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;

import fr.spideo.test.domain.Bidding;


public interface BiddingRepository {

    /**
     * Save the bidding in the acution with the giving id
     * 
     * @param bidding
     * @param auctionId
     * @return saved bigging
     * @throws IllegalArgumentException
     * @throws NoSuchElementException
     */
    Bidding save(Bidding bidding, Integer auctionId);

    /**
     * @param userName
     * @param auctionId
     * @return bidding with the giving user name for the giving auction id
     */
    Set<Bidding> finfByUserNameAndAuctionId(String userName, Integer auctionId);

    /**
     * @param auctionId
     * @return the bidding that have the max amount for the giving auction id
     */
    Optional<Bidding> findByAuctionIdAndMaxAmount(Integer auctionId);

}
