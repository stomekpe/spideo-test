package fr.spideo.test.repository;

import java.util.NoSuchElementException;
import java.util.Set;

import fr.spideo.test.domain.AuctionHouse;

public interface AuctionHouseRepository {

    /**
     * Save the giving auction house
     * 
     * @param auctionHouse
     * @return saved auction house
     * @throws IllegalArgumentException
     */
    AuctionHouse save(AuctionHouse auctionHouse);

    /**
     * @return all action houses
     */
    Set<AuctionHouse> getAll();

    /**
     * Delete the auction house with the giving id
     * 
     * @param id
     * @return
     * @throws NoSuchElementException
     * @throws IllegalArgumentException
     */
    Boolean deleteById(Integer id) ;

}
