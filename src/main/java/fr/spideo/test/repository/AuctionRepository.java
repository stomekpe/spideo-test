package fr.spideo.test.repository;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;

import fr.spideo.test.domain.Auction;
import fr.spideo.test.domain.AuctionStatusEnum;

public interface AuctionRepository {

    /**
     * Save the auction in the auction house with the giving auction house id
     * 
     * @param auction
     * @param auctionHouseId
     * @return saved auction
     * @throws IllegalArgumentException
     * @throws NoSuchElementException
     */
    Auction save(Auction auction, Integer auctionHouseId) ;

    /**
     * @param auctionHouseId
     * @return auctions in the auction house with the giving auction house id
     * @throws IllegalArgumentException
     */
    Set<Auction> findByAuctionHouseId(Integer auctionHouseId);

    /**
     * Update the auction status with DELETED status
     * 
     * @param id
     * @return the updated auction
     * @throws IllegalArgumentException
     */
    Optional<Auction> updateStatusToDeleted(Integer id) ;

    /**
     * @param status
     * @return all auction with the giving status
     */
    Set<Auction> findByStatus(AuctionStatusEnum status);

    /**
     * @param id
     * @return the auction with the giving id
     */
    Optional<Auction> findById(Integer id);

    /**
     * update the current price of the auction with the giving id
     * 
     * @param auctionId
     * @param currentPrice
     * @throws IllegalArgumentException
     * @throws NoSuchElementException
     */
    void updateAuctionCurrentPrice(Integer id, Long currentPrice) ;

}
