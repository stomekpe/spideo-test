package fr.spideo.test.config;

import java.util.HashSet;
import java.util.Set;

import fr.spideo.test.domain.AuctionHouse;
import lombok.Data;

@Data
public class AppData {
	
	Set<AuctionHouse> auctionHouses = new HashSet<>();

}
