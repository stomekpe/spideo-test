package fr.spideo.test.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class DataSourceConfig {
	
	@Bean
	public AppData dataSource(){
		return new AppData();
	}
}
