package fr.spideo.test.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ErrorMessageEnum {
    
    ID_NOT_NULL ("Id must be null"),
    AUCTION_ID_NULL("Auction id must not be null"),
    AUCTION_NOT_FOUND("Auction not found"),
    AUCTION_HOUSE_ID_NULL("Auction House id must not be null"),
    CURRENT_PRICE_NULL("Current price must not be null"),
    BIDDING_AMMOUNT_LOWER_THAN_CURRENT_PRICE("The bidding ammount is lower than auction current price"),
    START_PRICE_DIFFERENTE_CURRENT_PRICE("Start price must be same as current price"),
    START_TIME_AFTER_END_TIME("Start time cannot be after end one"),
    AUCTION_HOUSE_NOT_FOUND("Auction House not found"),
    AUCTION_NOT_RUNNING("Auction is not running"),
    AUCTION_NOT_TERMINATED("Auction is not terminated"),
    USER_NAME_NULL ("User name must be null"),
    STATUS_NULL ("Status must not be null");
   
    private String message;

}
