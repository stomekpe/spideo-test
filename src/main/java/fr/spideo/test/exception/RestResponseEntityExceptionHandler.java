package fr.spideo.test.exception;

import java.util.NoSuchElementException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({ BadRequestException.class, IllegalArgumentException.class })
    public ResponseEntity<RequestError> handleBadRequestExceptionException(Exception ex, WebRequest request) {
        log.error("error:",ex);
        return new ResponseEntity<RequestError>(new RequestError(ex.getMessage()), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ NoSuchElementException.class })
    public ResponseEntity<RequestError> handleNoSuchElementException(Exception ex, WebRequest request) {
        log.error("error:",ex);
        return new ResponseEntity<RequestError>(new RequestError(ex.getMessage()), new HttpHeaders(), HttpStatus.NOT_FOUND);
    }
    
    @ExceptionHandler({ Exception.class })
    public ResponseEntity<RequestError> handleOtherException(Exception ex, WebRequest request) {
        log.error("error:",ex);
        return new ResponseEntity<RequestError>(new RequestError(ex.getMessage()), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
    


    @RequiredArgsConstructor
    public class RequestError {

        @JsonProperty(value = "error-message")
        final String message;

    }

}
