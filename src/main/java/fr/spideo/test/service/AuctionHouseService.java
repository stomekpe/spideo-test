package fr.spideo.test.service;

import java.util.NoSuchElementException;
import java.util.Set;

import fr.spideo.test.domain.AuctionHouse;

public interface AuctionHouseService {

    /**
     * create auction house
     * 
     * @param auctionHouse
     * @return created auction house
     * @throws IllegalArgumentException
     */
    AuctionHouse create(AuctionHouse auctionHouse) throws IllegalArgumentException;

    /**
     * delete the auction house with the giving id
     * 
     * @param auctionHouseId
     * @throws NoSuchElementException
     * @throws IllegalArgumentException
     */
    void delete(Integer auctionHouseId) throws NoSuchElementException, IllegalArgumentException;

    /**
     * @return all auction house
     */
    Set<AuctionHouse> getAll();

}
