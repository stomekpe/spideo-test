package fr.spideo.test.service;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;

import fr.spideo.test.domain.Bidding;
import fr.spideo.test.exception.BadRequestException;

public interface BiddingService {

    /**
     * create a bidding in the auction with the giving id
     * 
     * @param bidding
     * @param auctionId
     * @return created bidding
     * @throws IllegalArgumentException
     * @throws BadRequestException
     * @throws NoSuchElementException
     */
    Bidding create(Bidding bidding, Integer auctionId) ;

  /**
   * 
   * @param userName
   * @param auctionId
   * @return biddings with the giving user name for the giving auction id
   * @throws NoSuchElementException
   * @throws IllegalArgumentException
   */
    Set<Bidding> findByUserNameAndAuctionId(String userName, Integer auctionId);

    /**
     * 
     * @param auctionId
     * @return the auction winner
     * @throws NoSuchElementException
     * @throws IllegalArgumentException
     * @throws BadRequestException
     */
    Optional<Bidding> getWinner(Integer auctionId) ;

}
