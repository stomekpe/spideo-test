package fr.spideo.test.service.impl;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;

import org.springframework.stereotype.Service;

import fr.spideo.test.domain.Auction;
import fr.spideo.test.domain.AuctionStatusEnum;
import fr.spideo.test.domain.Bidding;
import fr.spideo.test.exception.BadRequestException;
import fr.spideo.test.exception.ErrorMessageEnum;
import fr.spideo.test.repository.AuctionRepository;
import fr.spideo.test.repository.BiddingRepository;
import fr.spideo.test.service.BiddingService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class BiddingServiceImpl implements BiddingService {
    
    private final BiddingRepository biddingRepository;
    
    private final AuctionRepository auctionRepository;

    @Override
    public Bidding create(Bidding bidding, Integer auctionId) {
        Optional<Auction> auction = auctionRepository.findById(auctionId);
        if (!auction.isPresent()) {
            throw new NoSuchElementException(ErrorMessageEnum.AUCTION_NOT_FOUND.getMessage());
        }
        if (!AuctionStatusEnum.RUNNING.equals(auction.get().getStatus())) {
            throw new BadRequestException(ErrorMessageEnum.AUCTION_NOT_RUNNING.getMessage());
        }
        if(auction.get().getCurrentPrice()>=bidding.getAmount()){
            throw new BadRequestException(ErrorMessageEnum.BIDDING_AMMOUNT_LOWER_THAN_CURRENT_PRICE.getMessage());
        }
        auctionRepository.updateAuctionCurrentPrice(auctionId, bidding.getAmount());
        return biddingRepository.save(bidding, auctionId);
    }

    @Override
    public Set<Bidding> findByUserNameAndAuctionId(String userName, Integer auctionId) {
        Optional<Auction> auction = auctionRepository.findById(auctionId);
        if (!auction.isPresent()) {
            throw new NoSuchElementException(ErrorMessageEnum.AUCTION_NOT_FOUND.getMessage());
        }
        return biddingRepository.finfByUserNameAndAuctionId(userName, auctionId);
    }

    @Override
    public Optional<Bidding> getWinner(Integer auctionId) {
        Optional<Auction> auction = auctionRepository.findById(auctionId);
        if (!auction.isPresent()) {
            throw new NoSuchElementException(ErrorMessageEnum.AUCTION_NOT_FOUND.getMessage());
        }
        if (!AuctionStatusEnum.TERMINATED.equals(auction.get().getStatus())) {
            throw new BadRequestException(ErrorMessageEnum.AUCTION_NOT_TERMINATED.getMessage());
        }
        return biddingRepository.findByAuctionIdAndMaxAmount(auctionId);
    }

}
