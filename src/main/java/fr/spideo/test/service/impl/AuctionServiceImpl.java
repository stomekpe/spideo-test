package fr.spideo.test.service.impl;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;

import org.springframework.stereotype.Service;

import fr.spideo.test.domain.Auction;
import fr.spideo.test.domain.AuctionStatusEnum;
import fr.spideo.test.exception.BadRequestException;
import fr.spideo.test.exception.ErrorMessageEnum;
import fr.spideo.test.repository.AuctionRepository;
import fr.spideo.test.service.AuctionService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class AuctionServiceImpl implements AuctionService {
    
    private final AuctionRepository auctionRepository;

    @Override
    public Auction create(Auction auction, Integer auctionHouseId)  {
        if(!auction.getStartPrice().equals(auction.getCurrentPrice())){
            throw new BadRequestException(ErrorMessageEnum.START_PRICE_DIFFERENTE_CURRENT_PRICE.getMessage());
        }
        if(auction.getStartDateTime().isAfter(auction.getEndDateTime())){
            throw new BadRequestException(ErrorMessageEnum.START_TIME_AFTER_END_TIME.getMessage());
        }
        auction.setCurrentPrice(auction.getStartPrice());
        return auctionRepository.save(auction, auctionHouseId);
    }

    @Override
    public Auction updateStatusToDeleted(Integer auctionId)  {
        Optional<Auction> auction = auctionRepository.updateStatusToDeleted(auctionId);
        if (auction.isPresent()) {
            return auction.get();
        }
        throw new NoSuchElementException(ErrorMessageEnum.AUCTION_NOT_FOUND.getMessage());
    }

    @Override
    public Set<Auction> findByAuctionHouseId(Integer auctionHouseId) {
        return auctionRepository.findByAuctionHouseId(auctionHouseId);
    }

    @Override
    public Set<Auction> findByAuctionStatus(AuctionStatusEnum status) {
        return auctionRepository.findByStatus(status);
    }

}
