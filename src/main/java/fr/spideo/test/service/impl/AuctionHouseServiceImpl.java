package fr.spideo.test.service.impl;

import java.util.NoSuchElementException;
import java.util.Set;

import org.springframework.stereotype.Service;

import fr.spideo.test.domain.AuctionHouse;
import fr.spideo.test.exception.ErrorMessageEnum;
import fr.spideo.test.repository.AuctionHouseRepository;
import fr.spideo.test.service.AuctionHouseService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class AuctionHouseServiceImpl implements AuctionHouseService {

    private final AuctionHouseRepository auctionHouseRepository;

    @Override
    public AuctionHouse create(AuctionHouse auctionHouse) {
        return auctionHouseRepository.save(auctionHouse);
    }

    @Override
    public void delete(Integer auctionHouseId) {
        Boolean isdelete = auctionHouseRepository.deleteById(auctionHouseId);
        if (!isdelete) {
            throw new NoSuchElementException(ErrorMessageEnum.AUCTION_HOUSE_NOT_FOUND.getMessage());
        }
    }

    @Override
    public Set<AuctionHouse> getAll() {
        return auctionHouseRepository.getAll();
    }

}
