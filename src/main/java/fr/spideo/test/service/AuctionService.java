package fr.spideo.test.service;

import java.util.NoSuchElementException;
import java.util.Set;

import fr.spideo.test.domain.Auction;
import fr.spideo.test.domain.AuctionStatusEnum;
import fr.spideo.test.exception.BadRequestException;

public interface AuctionService {

    /**
     * Create the auction in the auction house with the giving auction house id
     * 
     * @param auction
     * @param auctionHouseId
     * @return created auction
     * @throws IllegalArgumentException
     * @throws BadRequestException
     * @throws NoSuchElementException
     */
    Auction create(Auction auction, Integer auctionHouseId);

    /**
     * Update the auction status with DELETED status
     * 
     * @param auctionId
     * @return the updated auction
     * @throws NoSuchElementException
     * @throws IllegalArgumentException
     */
    Auction updateStatusToDeleted(Integer auctionId);

    /**
     * @param auctionHouseId
     * @return all auction in the auction house with the giving auction house id
     * @throws IllegalArgumentException
     */
    Set<Auction> findByAuctionHouseId(Integer auctionHouseId) throws IllegalArgumentException;

    /**
     * @param status
     * @return auctions with the giving status
     * @throws IllegalArgumentException
     */
    Set<Auction> findByAuctionStatus(AuctionStatusEnum status) throws IllegalArgumentException;

}
