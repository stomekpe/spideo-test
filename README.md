# This is Semebia's implementation of Spideo Challenge

The project uses maven and is based on Spring MVC. It is a web project that can be deployed on tomcat server.

The API's documentation can be found on /v2/api-docs. Swagger-ui (path: /swagger-ui.html) is added for testing purposes.

Testing is done as functionnal tests. Unit test is for complex method.

